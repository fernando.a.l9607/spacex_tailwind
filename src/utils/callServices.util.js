import axios from 'axios';

// * SE CREA FUNCION CON METODO PST

export function postData(url,body){

    const config = {
        method: 'post',
        url: url,
        headers: { 
          'Content-Type': 'application/json'
        },
        data : body
      };
      
      return axios(config)
      .then(function (response) {
        return JSON.parse(JSON.stringify(response));
      })
      .catch(function (error) {
        return JSON.parse(JSON.stringify(error));
      });

}
