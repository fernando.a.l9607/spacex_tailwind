import React from 'react';
import './App.css';
import PastLaunches from "./components/pastLaunches/PastLaunches";
import NavBar from "./components/navBar/navBar";
import CompanyInfo from "./components/companyInfo/companyInfo"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
        <React.Fragment>
          <NavBar/> 
          <Switch>
            <Route  exact path="/missions"  component={PastLaunches}/>
            <Route  exact path="/company"  component={CompanyInfo}/>
          </Switch>
        </React.Fragment>
    </Router>
  );
}

export default App;
