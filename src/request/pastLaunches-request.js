export const POST_SPACEX_REQ = JSON.stringify({query: '{\n  launchesPast {\n    mission_name\n    launch_date_local\n    links {\n      wikipedia\n      flickr_images\n      article_link\n    }\n    rocket {\n      rocket_name\n      first_stage {\n        cores {\n          flight\n          land_success\n        }\n      }\n      second_stage {\n        payloads {\n          payload_type\n          payload_mass_kg\n          payload_mass_lbs\n        }\n      }\n    }\n    ships {\n      name\n      home_port\n      image\n    }\n    launch_success\n  }\n  company {\n    ceo\n    coo\n    cto_propulsion\n    cto\n    employees\n    founded\n    founder\n    launch_sites\n    name\n    summary\n    test_sites\n    valuation\n    vehicles\n    links {\n      twitter\n      website\n      flickr\n      elon_twitter\n    }\n    headquarters {\n      city\n      state\n      address\n    }\n  }\n}\n',
  variables: {}
  });

  export const POST_COMPANY_REQ = JSON.stringify({query: "{\n  company {\n    ceo\n    coo\n    cto_propulsion\n    cto\n    employees\n    founded\n    founder\n    launch_sites\n    name\n    summary\n    test_sites\n    valuation\n    vehicles\n  }\n}\n",
    variables: {}
  })

// export default POST_SPACEX_REQ;