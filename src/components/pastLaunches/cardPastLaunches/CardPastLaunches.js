import React, { Component, Fragment } from 'react';
import wikipedia  from './../../../assets/wikipedia.png';
import '../loader.css';

// import Link from 'next/link'


class CardPastLaunches extends Component {
  constructor () {
    super();
    this.state = {
      datos:"",
      flag:false,

    };
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.temporal !== ""){
      this.setState({datos: nextProps.temporal})
    }

    if (nextProps.flag === true) {
      this.setState({flag : true})
    }
  }

  componentWillMount(){
    
  }

  render() {
    return (
      <Fragment>
        {this.state.flag === true ? this.state.datos.map((item, count) => (
                <div key={"id" + count}  className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
                <article className="overflow-hidden rounded-lg shadow-lg">
                    <a href="#"> <img alt="Placeholder" className="block h-auto w-full" src={item.links.flickr_images[0]}/></a>
                    <header className="flex items-center justify-between leading-tight p-2 md:p-4">
                      <h1 className="text-lg"><p className={item.launch_success === true ? "no-underline text-black" : "no-underline text-black text-red-500"}>{item.mission_name}</p></h1>
                      <p className="text-grey-darker text-sm">{item.launch_date_local}</p>
                    </header>
                    <footer className="flex items-center justify-between leading-none p-2 md:p-4">
                        <a className="flex items-center no-underline hover:underline text-black" target="_blank" href={item.links.wikipedia}>
                            <img alt="Placeholder" className="block rounded-full" src={wikipedia}/>
                        </a>
                    </footer>
                    
                </article>
              </div>
          )) :""
        }
      </Fragment>
    )
     
  }

}

export default CardPastLaunches;
