
import React, { Component, Fragment } from "react";
import {postData} from '../../utils/callServices.util';
import {POST_SPACEX} from '../../constants/endPoints';
import {POST_SPACEX_REQ } from '../../request/pastLaunches-request';
import CardPastLaunches from '../pastLaunches/cardPastLaunches/CardPastLaunches';

class PastLaunches extends Component {
  constructor (props) {
    super(props);
    this.state = {
      datos:"",
      responSpacex: "",
      temporal:"",
      flag: false,
      num1:0,
      num2:12,
    };
  }


  componentDidMount(){
    this.getLanches();
  }

  getLanches(){
    postData(POST_SPACEX,POST_SPACEX_REQ).then(result =>{
      if(result!==null){
          this.setState({
            responSpacex: result.data.data,
            flag: true,
            temporal: result.data.data.launchesPast.slice(0, 12)
          })
      }	      
    });
  }
  sliceLessResult(){
    if(this.state.num2 > 12){
      const valorA = this.state.num1 - 12;
      const valorB = this.state.num2 - 12; 
    
      this.setState({
        temporal: this.state.responSpacex.launchesPast.slice(valorA,valorB),
        num1: valorA,
        num2: valorB,
      })
    }
  }
  sliceTemporalResult(){
    if(this.state.num2 < this.state.responSpacex.launchesPast.length){
      const valorA = this.state.num1 +12;
      const valorB = this.state.num2 +12; 
    
      this.setState({
        temporal: this.state.responSpacex.launchesPast.slice(valorA,valorB),
        num1: valorA,
        num2: valorB,
      })
    }
  }

  render() {
    return (
      <Fragment>
          <div className="container my-12 mx-auto px-4 md:px-12">
            <div className="flex flex-wrap -mx-1 lg:-mx-4">
             <CardPastLaunches flag = {this.state.flag} temporal={this.state.temporal}/>
            </div>
          </div>

          {this.state.flag === true ? <div className="container mx-auto p-6">
            <div className="flex justify-center">
              <a href="#" onClick={this.sliceLessResult.bind(this)} className="border rounded-lg rounded-l hover:bg-grey-lighter no-underline text-blue-darker p-3">&laquo;</a>
              <a href="#" className="p-3 border border-l-0 hover:bg-grey-lighter no-underline text-blue-darker">1</a>
              <a href="#" className="p-3 border border-l-0 hover:bg-grey-lighter no-underline text-blue-darker">2</a>
              <a href="#" className="p-3 border border-l-0 hover:bg-grey-lighter no-underline text-blue-darker">3</a>
              <a href="#" onClick={this.sliceTemporalResult.bind(this)} className="p-3 rounded-lg rounded-r border border-l-0 hover:bg-grey-lighter no-underline text-blue-darker">&raquo;</a>
            </div>
          </div>:
          <div className="flex object-center" id="loaderTopCard">
          <div className="object-center loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-64 w-64"></div>
        </div>}
      </Fragment>
    );
  }
}

export default PastLaunches;
