import React, { Component, Fragment } from 'react';
import {POST_SPACEX} from '../../constants/endPoints';
import {POST_COMPANY_REQ } from '../../request/pastLaunches-request';
import {postData} from '../../utils/callServices.util';
import './loaderInfo.css';



class CompanyInfo extends Component {
  constructor () {
    super();
    this.state = {
        responData: null,
        flag: false,
    };
  }

  componentDidMount(){
      this.getLanches()
  }

  getLanches(){
    postData(POST_SPACEX,POST_COMPANY_REQ).then(result =>{
      if(result!==null){
          this.setState({
            responData: result.data.data.company,
            flag: true,
          })
      }	      
    });
  }

  render() {
    return (
      <Fragment>
          {this.state.flag === true ?
                  <div className="min-h-screen bg-gray-300">
                  <div className="container mx-auto p-10 max-w-screen-lg">
                      <div className="bg-white rounded shadow p-8">
                          <h3 className="text-xl mt-4 font-bold">{this.state.responData.name}</h3>
                          <div className="border w-full rounded mt-5 flex p-4 justify-between items-center flex-wrap">
                                <div className="w-2/3">
                                    <h3 className="text-lg font-medium">Fundador: {this.state.responData.founder}</h3>
                                    <p className="text-gray-600 text-xs">ceo: <b>{this.state.responData.ceo}</b></p>
                                    <h4 className="text-blue-500	 text-xs font-bold mt-1">{this.state.responData.summary}</h4>
                                </div>
                              <div>
                                  <h4 className="text-3xl font-medium"><sup className="text-lg text-purple-800">Empleados: </sup>{this.state.responData.employees}</h4>
                                  <h5 className="text-sm font-bold text-purple-800">fecha de fundación: {this.state.responData.founded}</h5>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>:
              <div className="flex object-center" id="loaderTop">
                <div className="object-center loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-64 w-64"></div>
              </div>
          }
    </Fragment>
    )
     
  }

}

export default CompanyInfo;
